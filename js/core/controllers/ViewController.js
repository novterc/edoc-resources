'use strict';

angular.module('pixie.dashboard').controller('ViewController', ['$rootScope', '$scope', '$http', '$stateParams', '$state', '$mdDialog', 'fileTypes', 'rightPanel', 'utils', 'selectedItems', 'folders', 'localStorage', 'files', 'dashboardState', 'previewStatus', 'activity', 'users', 'mri', function($rootScope, $scope, $http, $stateParams, $state, $mdDialog, fileTypes, rightPanel, utils, selectedItems, folders, localStorage, files, dashboardState, previewStatus, activity, users, mri) {
    
    $rootScope.passContainerVisible = false;
    $rootScope.searchPanelShow = true;

    $scope.checkPassword = function() {
        var payload = {
            id: $state.params.id,
            type: $state.params.type,
            password: $scope.password
        };

        $http.post('shareable-password/check', payload).success(function() {
            selectedItems.password = $scope.password;
            $rootScope.sharePassword = $scope.password;
            $rootScope.passContainerVisible = false;
            localStorage.set('sharePasswords-'+payload.id, $scope.password );

        }).error(function(data) {
            $scope.error = data;
        })
    };



}]);