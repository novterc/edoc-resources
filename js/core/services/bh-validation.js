/**
 * Validation factory
 */
angular.module('pixie.dashboard')
    .factory('bh-validation', function () {

        var validation = new function () {

            /**
             * Appends validate messages
             * @param data - errors array
             * @param block - append block
             */
            this.validate = function (data, block) {
                $('.form-error').remove();

                $.each(data, function(field, message) {

                    //if there's no field name append error as alert before the first input field
                    if (field == '*') {
                        el.find('.alert').show().addClass('animated shake').find('.message').text(message);
                    } else {
                        var input = 'input[name="'+field+'"]';
                        if($(block + input).length == 0){
                            var input = 'textarea[name="'+field+'"]';
                            if($(block + input).length == 0){
                                var input = 'div[name="'+field+'"]';
                            }
                        }


                        $('<span class="form-error help-block">'+message+'</span>').insertAfter($(block + input).parent()).addClass('animated flipInX');
                    }
                });
            };
        };

        return validation;

    });