angular.module('app').directive('edBoxShowEditorSwitcher', function() {
	return {
		restrict: 'A',
    	require:'?ngModel',
        scope: {
            editorForModel: '=editorForModel',
        },
		link: function(scope, element) {

			var viewer = element.find('.pa-box-row-viewer');
			var editor = element.find('.pa-box-row-editor');
			var viewerSwitch = element.find('.pa-box-switch-viewer');
			var editorSwitch = element.find('.pa-box-switch-editor');
			var status = false;


			if( checkEmpty() )
				editorShow();
			else
				viewerShow();

			viewerSwitch.on('click', function(){ 
				viewerShow();
			});

			editorSwitch.on('click', function(){
				editorShow();
			});

			function editorShow(){
				editor.show();
				viewer.hide();
				viewerSwitch.show();
				editorSwitch.hide();
			}

			function viewerShow(){
				editor.hide();
				viewer.show();
				viewerSwitch.hide();
				editorSwitch.show();
			}

			function checkEmpty(){
				var data = scope.editorForModel;
				// debugger;
				if( Array.isArray(data) ) {
					for(var i in data) {
						var item = data[i];
						if( item !== undefined && item !== '' )
							return false;
					}
				} else {
					if( data !== undefined && data !== '' )
							return false;
				}

				return true;
			}
			
		}
	}
});