angular.module('app').directive('edHeightProgress', function() {
	return {
		restrict: 'A',
		scope: {
			edHeightProgress: "="
		},
		link: function($scope, el) {
			var value = 0;
			var valueOld = 0;
			var height = el.height();
			var onePercent = (height*0.01);
			update( value );

			// console.log(edHeightProgress);

			el.parents('form').on('change', function(){
				value = getPercentComplite($scope.edHeightProgress);
				update( value );
			});

			function update( value ){
				var colorHeight = Math.round(onePercent*value);
				el.find('.pa-color').css('height', colorHeight);
				el.find('.pa-digital').text(value+'%');

				// clearInterval(interval);
				// var valueOldUpdate = valueOld;
				// valueOld = value;
				// var change = value - valueOldUpdate;
				// var oneChange = Math.round(change * 0.10);
				// var int = 0;
				
				// var interval = setInterval(function(){
				// 	int++;
				// 	var v = valueOldUpdate+oneChange*int;
				// 	el.find('.pa-digital').text(v+'%');
				// 	if( int >= 10 ){
				// 		clearInterval(interval);
				// 		valueOldUpdate = value;
				// 	}
				// },80);
				
			}

			function getPercentComplite(modelArr) {
				var countComplite = 0;
				for(var key in modelArr) {
					if(!checkEmpty(modelArr[key])){
						countComplite++;
					}
				}
				var onePercent = 100/modelArr.length;
				return Math.round(onePercent*countComplite);
			}

			function checkEmpty(variable) {
				if( variable == undefined )
					return true;

				if( Array.isArray(variable) == true && variable.length == 0 )
					return true;

				if( typeof variable === 'object' && getLenghtObj(variable) == 0 )
					return true;

				if( variable == '' )
					return true;

				return false;
			}

			function getLenghtObj(obj) {
				var count = 0;
				for (var k in obj) {
				    if (obj.hasOwnProperty(k)) {
				       ++count;
				    }
				}
				return count;
			}
		}
	}
});