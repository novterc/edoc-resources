'use strict';

angular.module('pixie.dashboard').controller('ConsultationController', ['$rootScope', '$scope', '$http', 'dashboardState', 'users', '$mdDialog', 'bh-validation',
    function ($rootScope, $scope, $http, dashboardState, users, $mdDialog, bhvalidation) {

        var list_limit = 10;
        var doctor_limit = 10;
        $scope.doctor_id;

        var removeElementsByClass = function (className) {
            var elements = document.getElementsByClassName(className);
            while (elements.length > 0) {
                elements[0].parentNode.removeChild(elements[0]);
            }
        };

        /**
         * Filters and pagination
         */
        $scope.params = {
            itemsPerPage: '10',
            page: 1
        };

        $scope.search = {
            country: '',
            languages: '',
            specializations: '',
            directions: '',
            types: '',
            all: ''
        };

        $scope.$watchCollection('params', function () {
            getConsultationListFilter();
        });

        $scope.$watchCollection('search', function () {
            getConsultationListFilter();
        });

        var getConsultationListFilter = function () {
            $scope.searchData = {
                filter: $scope.search,
                pagination: $scope.params,
                doctor: $scope.selected_consultation
            };
            $http.post('consultationListFilter', $scope.searchData).success(function (data) {
                if ($scope.selected_consultation == 2) {
                    $scope.dcctor_consultations = data;
                }
                else {
                    $scope.consultations = data;
                }
            });
        };
        /**
         * End filters and pagination
         */


        /**
         * If user is doctor
         */
        if (users.current.isDoctor) {

            /**
             * Show our consultations
             *
             * @type {number}
             */
            $scope.selected_consultation = 2;

            /**
             * getting languages
             */
            $http.get('languages').success(function (responce) {
                $scope.languages = responce['data'];
            });

            /**
             * getting countries
             */
            $http.get('countries').success(function (responce) {
                $scope.countries = responce['data'];
            });

            /**
             * getting specializations
             */
            $http.get('specializations').success(function (responce) {
                $scope.specializations = responce['data'];
            });

            /**
             * getting directions
             */
            $http.get('directions').success(function (responce) {
                $scope.directions = responce['data'];
            });

            /**
             * getting consultation_types
             */
            $http.get('consultation_types').success(function (responce) {
                $scope.consultation_types = responce['data'];
            });

            /**
             * Show table for editing consultations
             *
             * @param $event
             * @param consultation
             */
            $scope.editConsultation = function ($event, consultation) {
                $scope.doc_consultation = {
                    action: 'update',
                    id: consultation.id,
                    doctor_id: consultation.doctor_id,
                    country: consultation.country,
                    directions: consultation.directions,
                    languages: consultation.languages,
                    specializations: consultation.specializations,
                    types: consultation.types
                };
                $scope.doc_consultation.action = 'update';
                var table = document.getElementById("add_consultation");
                if (!((' ' + table.className + ' ').indexOf('display_table') > -1)) {
                    table.className += " display-table";
                }
                if (document.getElementsByClassName('limit_error').length >= 1) {
                    removeElementsByClass('limit_error');
                }
                $scope.edit_type = 1;
            };

            /**
             * @type {number}
             */
            $scope.edit_type = 0;

            /**
             * Show table for creating new consultation
             */
            $scope.createNewConsultation = function () {
                var list = document.getElementById("your_consultations");
                list.className += 'hide-table';
                var table = document.getElementById("add_consultation");
                if (!((' ' + table.className + ' ').indexOf('display_table') > -1)) {
                    table.className += " display-table";
                    $scope.doc_consultation = {
                        action: 'add'
                    };
                    $scope.edit_type = 0;
                    if (document.getElementsByClassName('limit_error').length >= 1) {
                        removeElementsByClass('limit_error');
                    }
                }
            };

            /**
             * Removes chosen consultation
             */
            $scope.removeConsultation = function ($event, id) {
                $event.preventDefault();
                var data = {
                    'consultation_id': id
                };
                $http.post('removeDoctorConsultation', data).success(function (data) {
                }).success(function (responce) {
                    $scope.respoce = responce;
                    document.getElementById("add_consultation").className = "action-table ng-pristine ng-valid ng-submitted";
                    getDoctorsConsultations(doctor_limit);
                    getConsultationList(list_limit);
                });
            };

            /**
             * Hides table for new consultations
             * @param $event
             */
            $scope.hideNewConsultation = function ($event) {
                $event.preventDefault();
                document.getElementById("add_consultation").className = "action-table ng-pristine ng-valid ng-submitted";
            }
        }

        /**
         * Updates or adds doctor consultation info
         */
        $scope.addConsultation = function () {
            var form = $scope.doc_consultation;
            $http.post('add_consultation', form)
                .success(function (responce) {
                    $scope.respoce = responce;
                    document.getElementById("add_consultation").className = "action-table ng-pristine ng-valid ng-submitted";
                    getDoctorsConsultations(doctor_limit);
                    getConsultationList(list_limit);
                })
                .error(function (responce) {
                    bhvalidation.validate(responce, '.ng-valid ');
                });

        };

        $scope.showAnotherTable = function (val) {
            $scope.selected_consultation = val;
        };

        /**
         * Works with consultation request popup
         *
         * @param $event
         */
        $scope.showConsultationRequest = function ($event, doctor_id) {
            var options = {
                templateUrl: 'assets/views/modals/consultation-request.html',
                targetEvent: $event,
                locals: {activeTab: 'settings'},
                clickOutsideToClose: true,
                doctor_id: doctor_id,
                controller: ['$scope', '$http', function ($scope, $http, doctor_id) {
                    /**
                     * Hides popup
                     */
                    $scope.closeModal = function () {
                        $mdDialog.hide();
                    };

                    /**
                     * Sends request to create new consultation
                     */
                    $scope.sendConsultationRequest = function () {
                        if ($scope.consultation != '' && $scope.consultation != null && typeof $scope.consultation != 'undefined') {
                            $scope.consultation.doctor_id = options.doctor_id;
                        }
                        var consultation = $scope.consultation;
                        $http.post('createConsultation', consultation).success(function (data) {

                        }).error(function (data) {
                            bhvalidation.validate(data, '.md-default-theme ');
                        }).then(function (responce) {
                            $scope.consultation_request = responce['data'];
                        })
                    };

                }]
            };

            $mdDialog.show(options);
        };

        /**
         * Shows limit message to multiple selects
         *
         * @param $select
         * @param el
         */
        $scope.checkLimit = function ($select, el) {
            if ($select.selected.length >= $select.limit) {
                if (!document.querySelector('div[name=' + el.ngModel.$name + '] .limit_error')) {
                    var text = document.createElement('div');
                    text.className = 'limit_error form-error help-block animated flipInX';
                    text.innerHTML = "Maximum selected values is 3. Remove previous values to select another one.";
                    document.querySelector('div[name=' + el.ngModel.$name + ']').appendChild(text);
                }
            }
        };

        /**
         * Getting consultation list
         */
        $scope.totalItems = 0;
        var getConsultationList = function (limit) {
            var data = {
                limit: limit
            };
            list_limit = limit;
            $http.post('consultation_list', data).success(function (data) {
            }).then(function (responce) {
                $scope.consultations = responce['data'];
                if ($scope.consultations != '' && $scope.consultations != null) {
                    if ($scope.consultations[0] != '' && $scope.consultations[0] != null) {
                        $scope.totalItems = $scope.consultations[0].count;
                    }
                    else {
                        $scope.totalItems = 0;
                    }
                }
                else {
                    $scope.totalItems = 0;
                }

            });
        };

        /**
         * Getting doctor consultation list
         *
         * @param limit
         */
        $scope.totalPersonalItems = 0;
        var getDoctorsConsultations = function (limit) {
            var data = {
                limit: limit
            };
            doctor_limit = limit;
            $http.post('getDoctorConsultations', data).success(function (data) {
            }).then(function (responce) {
                $scope.dcctor_consultations = responce['data'];
                if ($scope.dcctor_consultations != '' && $scope.dcctor_consultations != null) {
                    if ($scope.dcctor_consultations[0] != '' && $scope.dcctor_consultations[0] != null) {
                        $scope.totalPersonalItems = $scope.dcctor_consultations[0].count;
                    }
                    else {
                        $scope.totalPersonalItems = 0;
                    }
                }
                else {
                    $scope.totalPersonalItems = 0;
                }
            });
        };

        getConsultationList(list_limit);
        getDoctorsConsultations(doctor_limit);

        $scope.setLimit = function (limit) {
            getConsultationList(limit);
        };

        $scope.setLimitPerson = function (limit) {
            getDoctorsConsultations(limit);
        };

        /**
         * Removes top menu
         */
        $rootScope.showSubNav = false;
        $rootScope.showNavBarSearch = false;
        $rootScope.showNavBarUploadButton = false;

        $scope.$on('$destroy', function () {
            $rootScope.showSubNav = true;
            $rootScope.showNavBarSearch = true;
            $rootScope.showNavBarUploadButton = true;
        });

        $scope.selected_consultation = 1;
        /**
         * End removes menu
         */

    }]);