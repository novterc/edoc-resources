'use strict';

angular.module('pixie.dashboard').controller('MriController', ['$rootScope', '$scope', '$state', '$http', 'mri', 'utils', function($rootScope, $scope, $state, $http, mri, utils) {

    $rootScope.$selectMriMenu = true;
    $rootScope.showSubNav = false;
    $rootScope.showNavBarSearch = false;
    $rootScope.showNavBarUploadButtonFolder = false;
    $rootScope.offSelectFolderTree();
    $scope.items = mri.items;
    $scope.mri = mri;

    mri.getMriItems().success(function() {
        if( checkLoadingItems($scope.items) )
                runUpdateInterval()
    })

    
    $scope.setUploadUrl( function () {
        return $scope.baseUrl + 'mri';
    });

    $scope.setUploadSuccess(function (data, historyItem) {  console.log( '============'); console.log( data );
        if (data.uploaded && data.uploaded.length) { 
            $scope.items = $scope.items.concat(data.uploaded);
            if( checkLoadingItems($scope.items) )
                runUpdateInterval()
            // historyItem.id = data.uploaded[0].id;
        }
    });


    var updateDataTimer = false;
    function runUpdateInterval(){
        var updateDataTimer = setInterval(function(){
            mri.getMriItems().success(function() {
                if( checkLoadingItems(mri.items) == false ){
                    clearTimeout(updateDataTimer);
                    $rootScope.$emit('activity.happened', 'uploaded', 'mri', {});
                }
            });
        }, 1000);        
    }

    function checkLoadingItems(items) {
        for (var key in items){
            var item = items[key];
            if( item.status < 9 )
                return true;
        }
        return false;
    }


    $scope.openPack = function(item) {
        if( item.status == 9 ){
            $state.go('dashboard.mriPhotos', { mriId: item.id });
        } else {
            utils.showToast('mri pack is not ready');
        }
    }


    $scope.$on('$destroy', function() {
        // unbind();        
        $rootScope.showSubNav = true;
        $rootScope.$selectMriMenu = false;
        $rootScope.showNavBarSearch = true;
        $rootScope.showNavBarUploadButtonFolder = true;
        clearTimeout(updateDataTimer);
        mri.setClearItems();
        $scope.setBasicUploadSuccess();
        $scope.setBasicUploadUrl();
        $rootScope.onSelectFolderTree();
    });
}]);