'use strict';

angular.module('pixie.dashboard').controller('MriPhotosController', ['$rootScope', '$scope', '$state', '$http', 'mri', 'dashboardState', function($rootScope, $scope, $state, $http, mri, dashboardState) {

    $rootScope.showSubNav = false;
    $rootScope.showNavBarSearch = false;
    $rootScope.showNavBarUploadButton = false;
    $rootScope.$selectMriMenu = true;
    $rootScope.snowRightCol = false;
    $rootScope.offSelectFolderTree();
    $rootScope.showNavBar = false;
    $scope.mri = mri;

    // dashboardState.loaded = false;    
 
     
    mri.getMriImages($state.params.mriId);

     $scope.$on('$destroy', function() {
        $rootScope.showSubNav = true;
        $rootScope.showNavBarSearch = true;
        $rootScope.showNavBarUploadButton = true;
        $rootScope.$selectMriMenu = false;
        $rootScope.snowRightCol = true;
        $rootScope.onSelectFolderTree();
        $rootScope.showNavBar = true;

        mri.setClearItems();
    });


}]);