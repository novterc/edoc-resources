angular.module('app').directive('edTooltipContent', function() {
    return {
        restrict: 'A',
        link: function($scope, el) {


            var carrier = el.find('.ed-carrier');
            var container = el.find('.ed-container');

            container.hide();


            carrier.tooltip({
                content: container.html(),       
            });
        
            // console.log( carrier );
            carrier.on('mouseover', function(){
                carrier.tooltip({
                    content: container.html(),       
                });
            });

            // $('.subitem').attr('title', $('.statusRollup').remove().html())
            // $(document).tooltip();

        }
    }
});