'use strict';

angular.module('pixie.dashboard').controller('NavbarController', ['$rootScope', '$scope', '$http', 'utils', 'selectedItems', 'previewStatus', '$state', function($rootScope, $scope, $http, utils, selectedItems, previewStatus, $state) {

    $scope.selectedItem = false;

    $scope.getSearchResults = function(query) {
        if( $state.is('view.filesfolders.folder') || $state.is('view.filesfolders.file') ){
            var params = {
                id: $state.params.id,
                type: $state.params.type,
                password: $scope.password
            }
            return $http.post('shareable/search/'+query, params).then(function(response) {
                $scope.searchResults = response.data;
                return response.data;
            });
        } else {
            return $http.get('search/'+query).then(function(response) {
                $scope.searchResults = response.data;
                return response.data;
            });
        }
    };

    $scope.goToSearchPage = function() {
        if ( !$scope.searchText ) return;

        utils.toState('dashboard.search', { query: $scope.searchText });
    };

    $scope.selectItem = function() {

        // debugger;
        if ( !$scope.selectedItem ) return;

        // console.log( selectedItems );
        selectedItems.set($scope.selectedItem, $scope.selectedItem.type, true);
        if(  $scope.selectedItem.type === 'file')
            previewStatus.open = true;
        else {
            selectedItems.preview();
        }
        $scope.searchText = '';
        $scope.selectedItem = false;
    };
}]);