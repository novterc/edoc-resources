'use strict';

angular.module('pixie.dashboard')
.controller('ConsultationsProfilController', ['$rootScope', '$scope', '$http', '$state', 'users', '$mdDialog', 'utils', function($rootScope, $scope, $http, $state, users, $mdDialog, utils) {
    
    var excludedDates = [];


    $scope.consultType2DatePicker1 = new Date();
    $scope.consultType2DatePicker2 = new Date();
    $scope.consultType2DatePicker3 = new Date();

    function setDatePicker($date) {
        var year = $date.getFullYear();
        var month = $date.getMonth();

        $scope.consultType2DatePicker1.setYear(year);
        $scope.consultType2DatePicker1.setMonth((month-1));

        $scope.consultType2DatePicker2.setYear(year);
        $scope.consultType2DatePicker2.setMonth(month);
        
        $scope.consultType2DatePicker3.setYear(year);
        $scope.consultType2DatePicker3.setMonth((month+1));
    }

    setDatePicker(new Date());

    $scope.$watch(
        function(scope) { 
            return moment($scope.consultType2DatePicker1 ).format('YYYY-MM-DD');
        },
        function(newValue, oldValue) {
            var m = moment($scope.consultType2DatePicker1);
            setDatePicker(m.add('months', 1).toDate());
        }
    );

    $scope.$watch(
        function(scope) { 
            return moment($scope.consultType2DatePicker2 ).format('YYYY-MM-DD');
        },
        function(newValue, oldValue) {
            var m = moment($scope.consultType2DatePicker2);
            setDatePicker(m.toDate());
        }
    );

    $scope.$watch(
        function(scope) { 
            return moment($scope.consultType2DatePicker3 ).format('YYYY-MM-DD');
        },
        function(newValue, oldValue) {
            var m = moment($scope.consultType2DatePicker3);
            setDatePicker(m.add('months', -1).toDate());
        }
    );



    

    $scope.consultType2MultiselDates = [];
    $scope.excludedDates = excludedDates;
    $scope.consult = {
        type_1:{
            options: {
                1: {},
                2: {},
                3: {},
            },
        },
        type_2:{
            dates:[],   
            lastSelectValue: false,      
        },
    };

    $scope.dateValue = new Date();

    var consultType2Dates = [];

    $scope.consult.type_2.onSelect = function(date) {
        // console.log('----select----');
        
        var convertDate = moment(date).format('YYYY-MM-DD');
        // console.log( convertDate );

        var findDateIndex = findInSubArray(consultType2Dates, 'date', convertDate );
        if( findDateIndex !== false ){
            var itemObj = consultType2Dates[findDateIndex];
        }else {
            var itemObj = {
                date: convertDate,
            }
            consultType2Dates.push(itemObj);
        }

        $scope.consult.type_2.lastSelectValue = itemObj;
        $scope.consult.type_2.dates.push(itemObj);
    }

    $scope.consult.type_2.onDisabled = function(date) {
        // console.log('----disabled----');
        // console.log( date );
        var convertDate = moment(date).format('YYYY-MM-DD');
        
        var findDateIndex = findInSubArray($scope.consult.type_2.dates, 'date', convertDate );
        $scope.consult.type_2.dates.slice(findDateIndex, 1);
        $scope.consult.type_2.lastSelectValue = false;
    }

    function findInSubArray(arr, fieldName, value ) {
        for( var key in arr ){
            if( arr[key][fieldName] == value )
                return key;
        }

        return false;
    }

    $http.get('consultationPriceGroup').success(function (data) {
    }).then(function (responce) {
        prices = responce['data'];

        for(var key in prices ){
            var item = prices[ key ];
            if( item.consultation_id == 11 )
                $scope.consult.type_1.options[1].examplePrice = item;
            if( item.consultation_id == 12)
                $scope.consult.type_1.options[2].examplePrice = item;
            if( item.consultation_id == 13 )
                $scope.consult.type_1.options[3].examplePrice = item;
            if( item.consultation_id == 21 )
                $scope.consult.type_2.examplePrice = item;
        }
    });


    var setExcludDates = function(arr) {
    	$scope.excludedDates = arr;
    }

    var getExcludeDates = function() {
    	return $scope.excludedDates;
    }

	$scope.openExcludedCalendarModal = function() {
		$mdDialog.show({
            template: $('#excluded-calendar-modal').html(),
            clickOutsideToClose: true,
            // targetEvent: $event,
            controller: ['$scope', function($scope) {
                $scope.create = true;
                $scope.dateMultiValue = getExcludeDates();
                $scope.select = function() {	
                    setExcludDates( $scope.dateMultiValue );
                    $mdDialog.hide();
                };
            }],
            disableParentScroll: false,
            onComplete: function() {
                console.log('complite');
            }
        });
	}

}]);
