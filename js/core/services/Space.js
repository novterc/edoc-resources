angular.module('app')

.factory('space', ['$rootScope', function($rootScope) {
    var spaceUse = 0;
    var space = {
    	set: function(i) {
    		space = i;
    	},

    	get: function() {
    		return space;
    	},

    	getAble: function() {
    		return (space.max_space - space.space_used);
    	},

    	check: function() {
    		if(this.getAble() > 0 )
    			return true;
    		else
    			return false;
    	}
	};

    return space;
}]);