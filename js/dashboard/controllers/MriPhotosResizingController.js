'use strict';

angular.module('pixie.dashboard').controller('MriPhotosResizingController', ['$rootScope', '$scope', 'mri', 'localStorage', function($rootScope, $scope, mri, localStorage) {
	
	$rootScope.ajaxProgress.files = true;
    $scope.mriViewMod = 'images';

    /*
        view mods: images, volume
    */
    

    mri.setPogressLoadCallback(function(proc){
        $scope.$apply(function() {
            $scope.loadProgress = proc;
        });
    });


    $rootScope.$on('mri.previewList.loaded', function() { 
    	setTimeout(function(){
	    	$scope.resizeCalc();
            $scope.normalZoom();
            $scope.setImageCenter();
	    }, 200);
    });

    $rootScope.$on('mri.serial.select', function() {
        // $scope.resizeCalc();
        $scope.normalZoom();
        // $scope.setImageCenter();
    });

    $scope.initItemList = function() {
        $rootScope.ajaxProgress.files = false;
        loadSizes();

        $scope.topEl.resizable();
        $scope.topEl.on('resize', $scope.topBottomResizingCalc);

        $scope.bottomLeftEl.resizable();
        $scope.bottomLeftEl.on('resize', $scope.leftRightResizingCalc);

        jQuery(window).resize(function(e, obj) {
        	if( obj == undefined ){
                $scope.resizeCalc();
                setTimeout(function(){
        		  $scope.resizeCalc(); 
                }, 200);       
            }
        });

        $scope.resizeCalc();
    }

    $scope.resizeCalc = function(){ 
        $scope.topBottomResizingCalc();
        $scope.leftRightResizingCalc();
    }

    function getSizeBlocks() {
    	var sizes = { topEl:{}, bottomEl: {}, bottomLeftEl: {}, bottomRightEl: {}, footerBarEl: {} };
    	sizes.topEl.height = $scope.topEl.height();
    	sizes.bottomEl.height = $scope.bottomEl.height();
    	sizes.bottomLeftEl.width = $scope.bottomLeftEl.width();
    	sizes.bottomRightEl.width = $scope.bottomRightEl.width();
        // sizes.footerBarEl.height = $scope.footerBarEl.height();
    	return sizes;
    }

    function setSizeBlocks(sizes) {
    	if(sizes == undefined )return;

    	if(sizes.topEl != undefined ) {
    		if(sizes.topEl.height != undefined )
    			$scope.topEl.height(sizes.topEl.height);
    	}
    	if(sizes.bottomEl != undefined ) {
    		if(sizes.bottomEl.height != undefined )
    			$scope.bottomEl.height(sizes.bottomEl.height);
		}
		if(sizes.bottomLeftEl != undefined ) {
    		if(sizes.bottomLeftEl.width != undefined )
    			$scope.bottomLeftEl.width(sizes.bottomLeftEl.width);
		}
		if(sizes.bottomRightEl != undefined ) {
    		if(sizes.bottomRightEl.width != undefined )
    			$scope.bottomRightEl.width(sizes.bottomRightEl.width);
    	}
        // if(sizes.footerBarEl != undefined ) {
        //     if(sizes.footerBarEl.height != undefined )
        //         $scope.footerBarEl.height(sizes.footerBarEl.height);
        // }
    }

    function saveSizes() {
    	var sizes = getSizeBlocks();
    	localStorage.set('MriPhotosResizingController-BlockSizes', sizes);
    }

    function loadSizes() {
    	var sizes = localStorage.get('MriPhotosResizingController-BlockSizes');
    	setSizeBlocks(sizes);
    }

    $scope.addEl = function(name, el) {
    	$scope[name] = el;
    }

    $scope.topBottomResizingCalc = function() {
    	var totalHeight = Number($scope.wrapperEl.height());
    	var topElHeihgt = Number($scope.topEl.height());
    	var correct = Number(2);
        debuggerScope.footerBarEl = $scope.footerBarEl;
        var footerBarElHeight = Number($scope.footerBarEl.outerHeight());
    	var bottomElHeight = (totalHeight-topElHeihgt-footerBarElHeight-correct);

    	$scope.bottomEl.css('height', bottomElHeight+'px');
    	saveSizes()
    }

    $scope.leftRightResizingCalc = function() {
        if( $scope.mriViewMod == 'images'){
        	var totalWidth = Number($scope.wrapperEl.width());
        	var bottomLeftElWidth = Number($scope.bottomLeftEl.width());
        	var correct = Number(5);
        	var bottomRightElWidth = (totalWidth-bottomLeftElWidth-correct);

        	$scope.bottomRightEl.css('width', bottomRightElWidth+'px');
        	saveSizes()
        }
    }

    $scope.selectImage = function(selectSerial, selectImage) {
        $scope.autoPlayImageStop();
        mri.selectImage(selectSerial, selectImage);
    }


    $scope.selectSerial = function(serialItem) {
        mri.selectSerial(serialItem);
        if( serialItem.nii == null )
            $scope.showImagesViewer();
    }

    $scope.whellDownImages = function() {
        $scope.autoPlayImageStop();
        $scope.moveSelectImage(true);
    }

    $scope.whellUpImages = function() {
        $scope.autoPlayImageStop();
        $scope.moveSelectImage(false);
    }

    $scope.autoPlayImageConf = {
        run: false,
        lastIndex: false,
        timeInterval: false,
    };

    $scope.autoPlayImageToggle = function() { 
        if( $scope.autoPlayImageConf.run === false ){
            $scope.autoPlayImageConf.run = true;
            $scope.autoPlayImageConf.timeInterval = setInterval(function(){
                if( mri.selectedSerial.loadingImagesPreviewStatus == true ){
                    return;
                }
                var index = false;
                $scope.$apply(function() {
                    index = $scope.moveSelectImage(true);
                });
                if( index === $scope.autoPlayImageConf.lastIndex ) {
                    $scope.moveSelectImageByIndex(0)
                    // $scope.autoPlayImageStop();
                } else {
                    $scope.autoPlayImageConf.lastIndex = index;
                }
            }, 100);
        } else {
            $scope.autoPlayImageStop();
        }
    }

    $scope.autoPlayImageStop = function() {
        $scope.autoPlayImageConf.run = false;
        $scope.autoPlayImageConf.lastIndex = false;
        clearInterval( $scope.autoPlayImageConf.timeInterval );
    }

    $scope.imageRangeMove = function(spin) {
        $scope.autoPlayImageStop();
        $scope.moveSelectImage(spin);
    }

    $scope.changeImageRange = function() {
        var index = Math.floor(Number(mri.seriesImageRange.value)/10);
        $scope.moveSelectImageByIndex( index );
        scrollChaseSelectElement();
    }

    $scope.moveSelectImage = function(spin) {
        if( mri.selectedImage.frames !== undefined || mri.selectedImage.frame_id !== null ){
            return $scope.moveSelectImageFrame(spin);
        }
        var lastIndex = mri.selectedSerial.images.length-1;
        var selectIndex = mri.selectedSerial.images.indexOf(mri.selectedImage);
        if( selectIndex == -1 )
                selectIndex = 0;
        var newSelectIndex = selectIndex;
        if( spin ) {
            if(selectIndex < lastIndex) {
                newSelectIndex = (selectIndex+1);
                mri.selectImage(mri.selectedSerial, mri.selectedSerial.images[newSelectIndex]);
            }
        } else {
            if(selectIndex > 0) {
                newSelectIndex = (selectIndex-1);
                mri.selectImage(mri.selectedSerial, mri.selectedSerial.images[newSelectIndex]);
            }
        }
        scrollChaseSelectElement();
        return newSelectIndex;
    }

    $scope.moveSelectImageByIndex = function(index) { 
        if( mri.selectedImage.frames !== undefined || mri.selectedImage.frame_id !== null ){
            mri.selectImageFrame(mri.selectedImageParent.frames[index]);
            mri.imageRangeMove(mri.selectedImageParent.frames, mri.selectedImageParent.frames[index]);
        } else {
            mri.selectImage(mri.selectedSerial, mri.selectedSerial.images[index]);
        }
    }

    $scope.moveSelectImageFrame = function(spin) {
        if( mri.selectedImage.frames !== undefined ){
            mri.selectedImageParent = mri.selectedImage;

            if( mri.selectedImageParent.loadedImagesFrame == undefined ) {
                mri.selectedImageParent.loadingImagesFrameStatus = true;
                mri.selectedImageParent.loadingImagesFrame = [];

                for(key in mri.selectedImageParent.frames) {
                    var imageItem = mri.selectedImageParent.frames[key];  
                    mri.preloaderImagesFromSerial_add(mri.selectedImageParent.loadingImagesFrame, mri.getImageSrc(imageItem), function(){
                        $rootScope.$apply(function() {
                            mri.selectedImageParent.loadedImagesFrame = true;
                            mri.selectedImageParent.loadingImagesFrameStatus = false; 
                        });
                    });
                }
            }

            mri.selectImageFrame(mri.selectedImage.frames[0]);
            var newSelectIndex = 0;
            // mri.imageRangeMove(mri.selectedImage.frames, mri.selectedImage.frames[0]);
        } else {

            var lastIndex = mri.selectedImageParent.frames.length-1;
            var selectIndex = mri.selectedImageParent.frames.indexOf(mri.selectedImage);
            if( selectIndex == -1 )
                selectIndex = 0;

            var newSelectIndex = selectIndex;
            if(spin){
                if(selectIndex < lastIndex) {
                    newSelectIndex = (selectIndex+1);
                    mri.selectImageFrame(mri.selectedImageParent.frames[newSelectIndex]);
                    mri.imageRangeMove(mri.selectedImageParent.frames, mri.selectedImageParent.frames[newSelectIndex]);
                }
            } else {
                if(selectIndex > 0) {
                    newSelectIndex = (selectIndex-1);
                    mri.selectImageFrame(mri.selectedImageParent.frames[newSelectIndex]);
                    mri.imageRangeMove(mri.selectedImageParent.frames, mri.selectedImageParent.frames[newSelectIndex]);
                }
            }

            return newSelectIndex;
        }
    }

    function scrollChaseSelectElement() {
        var parent = jQuery('.mri-photo-view-wrapper .mri-photo-view-list .view-list-item-wrapper');
        var child = jQuery('.mri-photo-view-wrapper .view-list-item.select');
    	var halfHeight = Math.floor(parent.height() / 2);

    	if( parent.width() < child.outerWidth() )
    		return;

    	var countInRow = Math.floor(parent.width() / child.outerWidth());
        var topPosition = (child.index() * (child.outerHeight() / countInRow));
        parent.scrollTop(topPosition-halfHeight);
    }

    $scope.changeZoom = function() {
        var zoomRange = mri.zoomRange;
    	if( $scope.imgEl == undefined )
    		return;

    	var width = $scope.imgEl.width();
        var height = $scope.imgEl.height();
    	var naturalWidth = $scope.imgEl[0].naturalWidth;
        var naturalHeight = $scope.imgEl[0].naturalHeight;
    	var newWidth = calcZoom(naturalWidth, zoomRange);
        var newHeight = calcZoom(naturalHeight, zoomRange);
    	$scope.imgEl.width(newWidth);


        var top = parseInt($scope.imgEl.css('top'));
        var left = parseInt($scope.imgEl.css('left'));
    	// newLeft = Math.round(calcZoom(left, zoomRange) / 2) ;

        var oldCenterWidth = Math.round(width/2)+left;
        var newCenterWidth = Math.round(newWidth/2)+left;
        // var newLeft = left+(oldCenterWidth-newCenterWidth);
        var blockWidth = $scope.bottomRightEl.width();

        var newLeft = left+Math.round((width-newWidth)/2);
        var newTop = top+Math.round((height-newHeight)/2);

    	// var top = $scope.imgEl.css('top');
    	// top = Math.round(calcZoom(top, zoomRange) / 2);

    	// console.log( newLeft );
    	// console.log( top );

    	$scope.imgEl.css('left', newLeft+'px');
        $scope.imgEl.css('top', newTop+'px');    
        $rootScope.$emit('mri.image.changeZoom');
    }

    function calcZoom(size, zoomRange) {
    	zoomRange = parseInt(zoomRange);
    	size = parseInt(size);
        return Math.round(size+((size/100)*zoomRange));
    }

    $scope.normalZoom = function() { 

        if( $scope.imgEl == undefined )
            return;

        // var width = $scope.imgEl.width();
        var naturalWidth = $scope.imgEl[0].naturalWidth;  
        var naturalHeight = $scope.imgEl[0].naturalHeight;  
        var blockWidth = $scope.bottomRightEl.width()-50;
        var blockHeight = $scope.bottomRightEl.height()-50;

        var newZoomWidth = Math.ceil( (blockWidth - naturalWidth ) / (naturalWidth/100) );
        var newZoomHeight = Math.ceil( (blockHeight - naturalHeight ) / (naturalHeight/100) );

        if( newZoomWidth < newZoomHeight )
            var newZoom = newZoomWidth;
        else
            var newZoom = newZoomHeight;
             
        // $scope.$apply(function(){
             mri.zoomRange = newZoom;
        // });
        $scope.changeZoom()
    }

    $scope.setImageCenter = function() { 
        var width = $scope.imgEl.width();
        var blockWidth = $scope.bottomRightEl.width();
        var newLeft = Math.round((blockWidth-width)/2)-30;
        $scope.imgEl.css('left', newLeft+'px');

        var height = $scope.imgEl.height();
        var blockHeight = $scope.bottomRightEl.height();
        var newTop = Math.round((blockHeight-height)/2)-30;
        $scope.imgEl.css('top', newTop+'px');
    }

    $scope.showVolumeViewer = function() {
        $scope.mriViewMod = 'volume';
    }

    $scope.showImagesViewer = function() {
        $scope.mriViewMod = 'images';
        setTimeout(function(){
            $scope.normalZoom();
            $scope.setImageCenter();
        }, 200);
    }

    $scope.changeImageContrast = function() {
        changeImageFilter();
    }

    $scope.changeImageBrightsness = function() {
        changeImageFilter();        
    }

    changeImageFilter = function() {
        $scope.imgEl.css('filter', 'contrast('+mri.imageContrast+'%) brightness('+mri.imageBrightsness+'%)');
    }

}]);