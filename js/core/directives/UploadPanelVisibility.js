'use strict';

angular.module('app').directive('edUploadPanelVisibility', ['$rootScope', function($rootScope) {
    return {
        restrict: 'A',
        link: function ($scope, el) {

            el.find('.close-panel').on('click', function() {
                $scope.$apply(function() {
                    el.removeClass('open');
                    $scope.uploadHistory = [];
                    $scope.uploadedFiles = [];
                    $scope.selectedFile = {};
                })
            });

            $rootScope.$on('upload.started', function() {
                console.log('upload.started ON');
                el.addClass('open');
            })
            
        }
    };
}]);