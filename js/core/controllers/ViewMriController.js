'use strict';

angular.module('app').controller('ViewMriController', [
        '$rootScope', 
        '$scope', 
        '$http', 
        '$stateParams', 
        '$state', 
        '$mdDialog', 
        'rightPanel', 
        'utils', 
        'mri', 
        'localStorage', 
        'dashboardState', 
        'previewStatus', 
        'activity', 
        'users', 
        function(
            $rootScope, 
            $scope, 
            $http, 
            $stateParams, 
            $state, 
            $mdDialog, 
            rightPanel, 
            utils,
            mri, 
            localStorage, 
            dashboardState, 
            previewStatus, 
            activity, 
            users
            
) {

    $rootScope.ajaxProgress.files = true;
    $rootScope.selectedItemsDownloadButton = false;
    $rootScope.searchPanelShow = false;
    $rootScope.showNavBar = false;
    $scope.$state  = $state;
    $scope.passContainerVisible = false;
    $scope.previewStatus = previewStatus;
    $scope.previewView = false;
    $scope.level = 1;
    $scope.utils = utils;
    $scope.mri = mri;


    mri.getShareMriImages($state.params.folderId, $state.params.id).error(function(){
        utils.showToast('Unable to open the MRI');
        $rootScope.ajaxProgress.files = false;
        // $state.go('home');
    });
    // mri.setDovElemForScrollChase('.mri-photo-view-wrapper .mri-photo-view-list', '.mri-photo-view-wrapper .view-list-item.select');

    $scope.initItemList = function() {
        $rootScope.ajaxProgress.files = false;
    }


    $scope.$on('$destroy', function() {
        $rootScope.shareable = false;
        mri.setClearItems();
    });
}]);