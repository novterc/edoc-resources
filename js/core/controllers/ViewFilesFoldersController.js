'use strict';

angular.module('pixie.dashboard').controller('ViewFilesFoldersController', ['$rootScope', '$scope', '$http', '$stateParams', '$state', '$mdDialog', 'fileTypes', 'rightPanel', 'utils', 'selectedItems', 'folders', 'localStorage', 'files', 'dashboardState', 'previewStatus', 'activity', 'users', function($rootScope, $scope, $http, $stateParams, $state, $mdDialog, fileTypes, rightPanel, utils, selectedItems, folders, localStorage, files, dashboardState, previewStatus, activity, users) {
    
    $scope.$state  = $state;
    $scope.selectedItems = selectedItems;
    $scope.previewStatus = previewStatus;
    $scope.level = 1;
    $scope.folders = folders;

    $scope.fileTypes = fileTypes;
    $scope.utils = utils;
    $scope.previewView = false;


    folders.getFolderByName = function(name) {
        var params = $state.params;
        params.folderId = name;
        return $http.post('shareable/preview', params).success(function(data) {
            if( $state.params.type == 'file' )
                assignShareable(data);
        });
    }

    folders.open = function(name) { 
        if (!name) return;
        $state.go('view.filesfolders.folder', { folderId: name });
    }

    //ads html trusted by angular
    if ( ! users.current.isSubscribed) {
        $scope.ad1 = utils.trustHtml(utils.getSetting('ad_preview_slot_1'));
        $scope.ad2 = utils.trustHtml(utils.getSetting('ad_preview_slot_2'));
    }


    $scope.dashboardState = dashboardState;

    if( $state.params.type === 'folder' ) {
        if ( ! folders.available.length) {
            folders.getShare();
        }
    } else {
        // $http.post('shareable/preview', $state.params).success(function(data) {
        //     assignShareable(data);
        // });

        //if we already have a shareable on rootScope preview that
        if ($rootScope.shareable && users.current) { 
            assignShareable($rootScope.shareable);

        //otherwise fetch shareable from server based on state params
        } else {
            $http.post('shareable/preview', $state.params).success(function(data) {
                assignShareable(data);
            }).error(function() {
                if (users.current) {
                    // utils.toState('dashboard.folders');
                } else {
                    // utils.toState('home');
                }
            }).finally(function() {
                $rootScope.ajaxProgress.files = false;
            });
        }
    }

    // console.log( folders.available.length );
    //



    function assignShareable(shareable) { 
        $scope.shareable = shareable;
        $scope.type = shareable.type;

        //ask for password if it's not currently logged in users file and it's password protected
        if (shareable.password && (! users.current || users.current.id != shareable.user_id)) {
            $scope.passContainerVisible = true;
        }

        selectedItems.setAll(shareable.files || shareable);
        $scope.previewView = true;
        $scope.previewStatus = { open: true };
        $scope.$broadcast('shareable.ready', shareable);
    }

    $scope.toDashboard = function() {
        utils.toState('dashboard.folders');
    };

    $scope.goHome = function() {
        utils.toState('home');
    };


    $scope.getSize = function() {
        if ($scope.shareable) {
            var size = $scope.shareable.type === 'file' ? utils.formatFileSize($scope.shareable.file_size) : utils.getFolderSize($scope.shareable);

            if (size) {
                return size;
            } else {
                return '';
            }
        }
    };

    $scope.$on('$destroy', function() {
        $rootScope.shareable = false;
        selectedItems.deselectAll();
    });
}])

.value('dashboardState', { loaded: false })
.value('previewStatus', { open: false });   