'use strict';

angular.module('pixie.dashboard').factory('mri', ['$rootScope', '$http', 'utils', '$state', 'localStorage', function($rootScope, $http, utils, $state, localStorage) {
    var mri = {

        /**
         * If we have fetched mri from server already.
         */
        loaded: false,
        imagesLoaded: {},

        /**
         * Items (files and folders) user has favorited.
         */
        items: [],
        serials: [],
        images: [],

        selectedSerial: false, 
        selectedImage: false,
        selectedByListImage: false,
        isFrame: false,

        showPreviewsPanel: false,
        zoomRange: 0,
        seriesImageRange: {
            value: 0,
            min: 0,
            max: 0,
            count: 0,
            selIndex: 0,
        },
        selectedImageParent: false,

        imageContrast: 100,
        imageBrightsness: 100,

        /**
         * Return a favorited item by id.
         *
         * @param {string|int} id
         * @returns {object|undefined}
         */
        getItemById: function (id) {
            for (var i = 0; i < this.items.length; i++) {
                if (this.items[i].id == id) {
                    return this.items[i];
                }
            }
        },

        /**
         * Get user favorited items.
         *
         * @returns {promise}
         */
        //getFavoritedItems: function() {
        //    var self = this;
        //
        //    return $http.get($rootScope.baseUrl+'labels/favorite').success(function(data) {
        //        self.items = data;
        //        self.loaded = true;
        //    });
        //}


        getMriItems: function () {
            var self = this;

            return $http.get($rootScope.baseUrl + 'mri').success(function (data) {
                self.items = data;
                self.loaded = true;
            }).error(function(data) {

                if (angular.isString(data) && data.length < 300) {
                    utils.showToast(data);
                }
            });
        },


        getMriImages: function($id) {
            var self = this;
            return $http.get($rootScope.baseUrl + 'mri/'+$id).success(function (data) {
                self.serials = data;
                self.selectSerial( self.serials[0] );
            });
        },

        getShareMriImages: function($id, $shareId) {
            var self = this;
            
            var $params = {
                id: $id,
                share_id: $shareId,
            };

            if( !$rootScope.sharePassword ) {
                $params.password = localStorage.get('sharePasswords-'+$params.share_id);
            } else {
                $params.password = $rootScope.sharePassword;
            }

            return $http.post('shareable/mri', $params).success(function(data) {
                if(data=='getPassword'){
                    $rootScope.passContainerVisible = true;
                } else {
                    self.serials = data;
                    if( self.serials[0] == undefined ){
                    $rootScope.passContainerVisible = false;
                        utils.showToast('mri pack is not ready');
                        return ;
                    }
                    self.selectSerial( self.serials[0] );                    
                }
            })
        },

        setClearItems: function() {
            this.items = [];
            this.serials = [];
            this.selectedSerial = false; 
            this.selectedImage = false;

        },

        setPogressLoadCallback: function(call){
            this.progressLoadCall = call;
        },

        preloaderImagesFromSerial: function(serialItem) {
            if( serialItem.loadingImagesPreviewStatus == false )
                return;
            
            serialItem.loadingImagesPreviewStatus = true;
            serialItem.loadingImagesPreview = [];
            serialItem.loadProgress = 0;

            serialItem.loadingImagesStatus = true;
            serialItem.loadingImages = [];

            var dsadasd = this;
            var onePercent = Math.round(serialItem.images.length*0.01);

            for(key in serialItem.images) {
                var imageItem = serialItem.images[key];  
                this.preloaderImagesFromSerial_add(serialItem.loadingImagesPreview, this.getImagePreviewSrc(imageItem), function(){
                    $rootScope.$apply(function() {
                        serialItem.loadingImagesPreviewStatus = false;
                        $rootScope.$emit('mri.previewList.loaded');
                    });
                });
                this.preloaderImagesFromSerial_add(serialItem.loadingImages, this.getImageSrc(imageItem), function(){
                    $rootScope.$apply(function() {
                        serialItem.loadingImagesStatus = false;
                        $rootScope.$emit('mri.imagesSeries.loaded');
                    });
                }, function(length){
                    var proc = Math.round(100-length/onePercent);
                    dsadasd.progressLoadCall(proc);
                });
            }
        },

        preloaderImagesFromSerial_add: function(loadingImagesArray, src, compliteCall, progressCall) {
            var image = new Image();
            loadingImagesArray.push(image);
            image.onload = function(v1, v2){
                var index = loadingImagesArray.indexOf(this);
                loadingImagesArray.splice(index, 1);

                if( progressCall !== undefined )
                    progressCall(loadingImagesArray.length);

                if( loadingImagesArray.length == 0 ){
                    compliteCall();
                }
            }
            image.src = src;
        },

        getImagePreviewSrc: function(imageItem) {
            return '/assets/uploads_mri/'+imageItem.user_id+'/'+imageItem.mri_id+'/'+imageItem.share_id+'_thumb70x70.png';
        },

        getImageSrc: function(imageItem) {
            return '/assets/uploads_mri/'+imageItem.user_id+'/'+imageItem.mri_id+'/'+imageItem.share_id+'.png';
        },

        getVolumeNiiSrc: function(serial) {
            return '/brainbrowser-master/examples/test.html?nii=/assets/uploads_mri/'+serial.user_id+'/'+serial.mri_id+'/'+serial.nii;
        },


        selectSerial: function(serialItem) { 
            this.isFrame = false;
            this.preloaderImagesFromSerial(serialItem);
            this.selectedSerial = serialItem;
            this.selectedImage = serialItem.images[0];
            this.imageRangeMove(serialItem.images);
            $rootScope.$emit('mri.image.select');
            $rootScope.$emit('mri.serial.select');
        },

        selectImage: function(serialItem, image) {
            this.isFrame = false;
            if( image.frames !== undefined )
                this.isFrame = true;
            this.selectedSerial = serialItem;
            this.selectedImage = image;
            this.selectedByListImage = image;
            this.selectedImageParent = false;
            this.imageRangeMove(serialItem.images, image);
            $rootScope.$emit('mri.image.select');
        },

        selectImageFrame: function(image) {
            this.isFrame = true;
            this.selectedImage = image;
        },

        toggleSerial: function(serialItem) {
            if(serialItem.open == true)
                serialItem.open = false;
            else 
                serialItem.open = true;
        },

        imageRangeMove: function(list, selItem) {
            this.seriesImageRange.count = list.length;
            this.seriesImageRange.max = ((Number(list.length)-1)*10);
            var selectIndex = list.indexOf(selItem);
            if( selectIndex < 0 )
                selectIndex = 0;
            this.seriesImageRange.selIndex = selectIndex+1;
            this.seriesImageRange.value = (Number(selectIndex)*10);
        },
       

    };


    $rootScope.$on('user.loggedOut', function() {
        mri.setClearItems();
    });

    return mri;
}]);