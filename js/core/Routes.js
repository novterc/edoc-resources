'use strict';
angular.module('app')
.config(['$urlRouterProvider', '$stateProvider', '$urlMatcherFactoryProvider', '$locationProvider', function($urlRouterProvider, $stateProvider, $urlMatcherFactoryProvider, $locationProvider) {

    //enable html5 pushState if user selected it
    if (parseInt(vars.settings.enablePushState)) {
        $locationProvider.html5Mode(true);
    }

    $urlMatcherFactoryProvider.strictMode(false);

    $urlRouterProvider.when('/dashboard', '/dashboard/folders');
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'assets/views/home.html',
            controller: 'HomeController'
        })


        .state('login', {
            url: '/login',
            templateUrl: 'assets/views/login.html',
            controller: 'LoginController'
        })
        .state('register', {
            url: '/register',
            templateUrl: 'assets/views/register.html',
            controller: 'RegisterController'
        })


        //ACCOUNT
        .state('account', {
            url: '/account',
            abstract: true,
            templateUrl: 'assets/views/account/account.html',
            controller: 'AccountController'
        })
        .state('account.profil', {
            url: '/profil',
            templateUrl: 'assets/views/account/profil.html',
            controller: 'ProfilController'
        })
        .state('account.consultationsProfil', {
            url: '/consultations/profil',
            templateUrl: 'assets/views/account/consultations/profil.html',
            controller: 'ConsultationsProfilController'
        })


        //DASHBOARD
        .state('dashboard', {
            url: '/dashboard',
            abstract: true,
            templateUrl: 'assets/views/dashboard/dashboard.html',
            controller: 'DashboardController'
        })
        .state('dashboard.trash', {
            url: '/trash',
            templateUrl: 'assets/views/dashboard/files.html',
            controller: 'TrashController'
        })
        .state('dashboard.recent', {
            url: '/recent',
            templateUrl: 'assets/views/dashboard/files.html',
            controller: 'RecentController'
        })
        .state('dashboard.doctor_consultation', {
            url: '/doctor_consultation',
            templateUrl: 'assets/views/dashboard/doctor_consultation.html',
            controller: 'ConsultationController'
        })
        .state('dashboard.favorites', {
            url: '/favorites',
            templateUrl: 'assets/views/dashboard/files.html',
            controller: 'FavoritesController'
        })
        .state('dashboard.search', {
            url: '/search/:query',
            templateUrl: 'assets/views/dashboard/files.html',
            controller: 'SearchController'
        })
        .state('dashboard.folders', {
            url: '/folders/:folderId?',
            templateUrl: 'assets/views/dashboard/files.html',
            params: {
                folderName: { value: null }
            }
        })
        .state('dashboard.foldersRoot', {
            url: '/folders',
            templateUrl: 'assets/views/dashboard/files.html',
            params: {
                folderName: { value: null }
            }
        })


        //MRI
        .state('dashboard.mri', {
            url: '/mri',
            templateUrl: 'assets/views/dashboard/mri.html',
            controller: 'MriController'
        })
        .state('dashboard.mriPhotos', {
            url: '/mri/:mriId',
            templateUrl: 'assets/views/dashboard/mri_photos.html',
            controller: 'MriPhotosController'
        })


        //SHARE
        .state('view', {
            url: '/view',
            abstract: true,
            templateUrl: 'assets/views/view.html',
            controller: 'ViewController'
        })
        .state('view.filesfolders', {
            url: '/:id/:folderId',
            abstract: true,
            templateUrl: 'assets/views/view-filesfolders.html',
            controller: 'ViewFilesFoldersController'
        })
        .state('view.filesfolders.folder', {
            url: '/folder',
            templateUrl: 'assets/views/dashboard/files.html',
            params: {
                type: 'folder',
                folderName: { value: null }
            }
        })
        .state('view.filesfolders.file', {
            url: '/file',
            templateUrl: 'assets/views/view-preview.html',
            params: {
                type: 'file',
                folderName: { value: null }
            }
        })
        .state('view.mri', {
            url: '/:id/:folderId/mri',
            templateUrl: 'assets/views/dashboard/mri_photos.html',
            controller: 'ViewMriController',
            params: {
                type: 'mri',
                folderName: { value: null }
            }
        })

        
        //ADMIN
        .state('admin', {
            url: '/admin',
            abstract: true,
            templateUrl: 'assets/views/admin/admin.html',
            controller: 'AdminController',
        })
        .state('admin.users', {
            url: '/users',
            templateUrl: 'assets/views/admin/users.html'
        })
        .state('admin.files', {
            url: '/files',
            templateUrl: 'assets/views/admin/files.html'
        }).state('admin.folders', {
            url: '/folders',
            templateUrl: 'assets/views/admin/folders.html'
        })
        .state('admin.analytics', {
            url: '/analytics',
            templateUrl: 'assets/views/admin/analytics.html',
            controller: 'AnalyticsController'
        })
        .state('admin.settings', {
            url: '/settings',
            templateUrl: 'assets/views/admin/settings.html',
            controller: 'SettingsController'
        })
        .state('admin.ads', {
            url: '/ads',
            templateUrl: 'assets/views/admin/admin-ads.html',
            controller: 'SettingsController'
        })


}]);
