var app = angular.module('app');

app.directive('edPositiveInteger', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
                var INTEGER_REGEXP = /^\d+$/;
                if (INTEGER_REGEXP.test(viewValue)) { // it is valid
                    ctrl.$setValidity('positiveInteger', true);
                    return viewValue;
                } else { // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('positiveInteger', false);
                    return undefined;
                }
            });
        }
    };
});


app.directive('edDateFormat', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
                var INTEGER_REGEXP = /^[0-9]{4}\.[0-9]{1,2}\.[0-9]{1,2}$/;
                if (INTEGER_REGEXP.test(viewValue)) { // it is valid 
                
                    var unixTime = Date.parse(viewValue);
                    if(isNaN(unixTime)){
                        ctrl.$setValidity('dateFormatCus', false);
                        return undefined;
                    }
                    
                    ctrl.$setValidity('dateFormatCus', true);
                    return viewValue;
                } else { // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('dateFormatCus', false);
                    return undefined;
                }
            });
        }
    };
});


app.directive('edLatinicChars', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
                var INTEGER_REGEXP = /^[A-Za-z]*$/;
                if (INTEGER_REGEXP.test(viewValue)) { // it is valid 
                    ctrl.$setValidity('dateFormatCus', true);
                    return viewValue;
                } else { // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('dateFormatCus', false);
                    return undefined;
                }
            });
        }
    };
});



app.directive('edPhoneFormat', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
                var INTEGER_REGEXP = /^\+?[0-9\-]*$/;
                if (INTEGER_REGEXP.test(viewValue)) { // it is valid
                    ctrl.$setValidity('positiveInteger', true);
                    return viewValue;
                } else { // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('positiveInteger', false);
                    return undefined;
                }
            });
        }
    };
});

