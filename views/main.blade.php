<!DOCTYPE html>
<html>
    <head>

        {{-- Set base url if admin has enabled HTML5 push state --}}
        @if ($settings->get('enablePushState'))
            <base href="{{ $pushStateRootUrl }}">
        @endif

        <title>{{ $settings->get('siteName') }}</title>

        {{-- Meta --}}
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="{{ $settings->get('metaDescription') }}" />
        <meta name="title" content="{{ $settings->get('metaTitle') }}" />

        {{-- CSS For Splash Spinner --}}
        <style>[ng-cloak]#splash{display:block!important}[ng-cloak]{display:none}#splash{display:none;position:absolute;top:45%;left:48%;width:50px;height:50px;z-index:0;animation:loader 2s infinite ease;border:4px solid #ff5722}#splash-spinner{vertical-align:top;display:inline-block;width:100%;background-color:#ff5722;animation:loader-inner 2s infinite ease-in}@keyframes loader{0%{transform:rotate(0deg)}25%,50%{transform:rotate(180deg)}100%,75%{transform:rotate(360deg)}}@keyframes loader-inner{0%,25%{height:0}50%,75%{height:100%}100%{height:0}}</style>
        {{-- CSS --}}
        <link rel="stylesheet" href="{{ asset('assets/css/styles.min.css?v12') }}">
        {{-- Fonts --}}
        <link href='https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,900' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/account.css') }}">

        {{-- Favicons --}}

        <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">

        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="manifest" href="favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <script type="text/javascript">
            var debuggerScope = {};
        </script>
    </head>

    <body ng-app="app">

        <div id="splash" ng-cloak>
            <div id="splash-spinner"></div>
        </div>

        <div ng-cloak id="total-app-wrapper" style="height: 100%" ng-controller="RootController">
            <div id="main-view" ui-view></div>
        </div>

        <script src="{{ asset('assets/js/core.min.js?v=1') }}"></script>
        <script id="vars">
            var vars = {
                user: '{!! $user !!}',
                baseUrl: '{{ $baseUrl  }}',
                selectedLocale: '{{ Config::get('app.locale') }}',
                trans: {!! $translations !!},
                settings: {!! json_encode($settings->getAll()) !!},
                isDemo: '{{ $isDemo  }}'
            }

            $( function() {
                // $.widget("ui.tooltip", $.ui.tooltip, {
                //     options: {
                //         content: function () {
                //             return $(this).prop('title');
                //         }
                //     }
                // });
                $( document ).tooltip();
            });
        </script>


        @if (($locale = $settings->get('dateLocale', 'en')) && $locale !== 'en')
            <script src="{{ asset('assets/js/locales/'.$locale.'.js')  }}"></script>
        @endif

        @if ($code = $settings->get('analytics'))

        @endif
    </body>
</html>
