'use strict';
angular.module('app')

.directive('edHideRange', ['$rootScope', function($rootScope) {
    return function(scope, element, attrs, ngModelCtrl) {
        element.bind("DOMMouseScroll mousewheel onmousewheel", function(event) { 
               
            // cross-browser wheel delta
            var event = window.event || event; // old IE support
            var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
    
            var range = $(element).find('input');
            var min = parseInt(range.attr('min'));
            var max = parseInt(range.attr('max'));
            var value = parseInt(range.val());
            var step = 2;


            if(delta > 0) { 
                if( value < max )
                    value = (value+step);
            } else {
                if( value > min )
                    value = (value-step);
            }

            range.val(value);

            range.change();


            event.stopPropagation();
        });
    };
}])