'use strict';

angular.module('app')
.controller('RegisterController', ['$rootScope', '$scope', '$http', '$state', 'users', '$mdDialog', 'utils', function($rootScope, $scope, $http, $state, users, $mdDialog, utils) {
    
    console.log('RegisterController init');

    $scope.credentials = {
        user_type: 'pacient',
    };

    $scope.credentials.consult = {
        type: {
            1: {
                switch: false,
                options: {
                    1: {
                        switch: false,
                    },
                    2: {
                        switch: false,
                    },
                    3: {
                        switch: false,
                    },
                },
            },
            2: {
                switch: false,

            },
        },
    };

    $scope.credentials.countConsultWeek = [];
    $scope.credentials.files = [];

    $scope.organizerTypeName = {
        1: 'One date',
        2: 'Period',
        3: 'every week',
        4: 'every month ',
        5: 'every month',
        6: 'every year',
    }

    var weekDayName = {
                    0: 'Mo',
                    1: 'Tu',
                    2: 'We',
                    3: 'Th',
                    4: 'Fr',
                    5: 'Sa',
                    6: 'Su',
                };

    var avatar = {
        src_default: 'http://localhost:96/assets/images/avatars/male.png',
        src: false,
        loadData: false,
    }    

    var teleconference = [];
    $scope.teleconference = teleconference;

    var organizerModel = {
        items: [],
        addDate: function(obj) {
            this.items.push(obj);
        },
        formatDate: function(date) {
            return moment(date).format('YYYY MMMM D');
        },
        classes: {
            type_1: function(date){
                // one date
                this.typeNumber = 1;
                this.data = { date: date };
                this.showTitle = function(){
                    return organizerModel.formatDate(this.data.date);
                }

                return this; 
            },
            type_2: function(date1, date2){
                // period
                this.typeNumber = 2;
                this.data = { date1: date1, date2: date2 };
                this.showTitle = function(){
                    return organizerModel.formatDate(this.data.date1)+' - '+organizerModel.formatDate(this.data.date2);
                }

                return this; 
            },
            type_3: function(weekNums){
                // interval week day
                this.typeNumber = 3;
                this.data = { weekNums: weekNums };
                this.showTitle = function(){
                    var strTitle = '';
                    for( var key in this.data.weekNums ){
                        strTitle = strTitle+weekDayName[this.data.weekNums[key]]+' ';
                    }
                    return strTitle;
                }

                return this; 
            },
            type_4: function(numDay){
                // interval month day
                this.typeNumber = 4;
                this.data = { numDay: numDay };
                this.showTitle = function(){
                    var strTitle = '';
                    for( var key in this.data.numDay ){
                        strTitle = strTitle + this.data.numDay[key] + ' ';
                    }
                    return strTitle;
                }

                return this; 
            },
            type_5: function(list){
                // interval month week day
                this.typeNumber = 5;
                this.data = { list: list };
                this.showTitle = function(){
                    var strTitle = '';
                    for( var key in this.data.list ){
                        var item = this.data.list[key];
                        strTitle = strTitle + 'week ' + item[0] + ' ' + weekDayName[ item[1] ] +', ';
                    }
                    return strTitle;
                }

                return this; 
            },            
            type_6: function(date1, date2){
                // period
                this.typeNumber = 6;
                this.data = { date1: date1, date2: date2 };
                this.showTitle = function(){
                    return moment(this.data.date1).format('MMMM D')+' - '+moment(this.data.date2).format('MMMM D')
                }

                return this; 
            },
        },
    }


    
    var AKJDHsakdha = $scope.credentials.files;
    $rootScope.$on('files.uploaded', function(v1, v2) {
        AKJDHsakdha.push(v2);
    });

    $http.get('specializations').success(function (data) {
    }).then(function (responce) {
        $scope.specializations = responce['data'];
    });

    $http.get('languages').success(function (data) {
    }).then(function (responce) {
        $scope.languages = responce['data'];
    });

    $http.get('countries').success(function (data) {
    }).then(function (responce) {
        $scope.countries = responce['data'];
    });

    $http.get('workings').success(function (data) {
    }).then(function (responce) {
        $scope.workings = responce['data'];
    });

    $http.get('workings').success(function (data) {
    }).then(function (responce) {
        $scope.workings = responce['data'];
    });

    $http.get('consultationPriceGroup').success(function (data) {
    }).then(function (responce) {
        prices = responce['data'];

        for(var key in prices ){
            var item = prices[ key ];
            if( item.consultation_id == 11 )
                $scope.credentials.consult.type[1].options[1].examplePrice = item;
            if( item.consultation_id == 12)
                $scope.credentials.consult.type[1].options[2].examplePrice = item;
            if( item.consultation_id == 13 )
                $scope.credentials.consult.type[1].options[3].examplePrice = item;
            if( item.consultation_id == 21 )
                $scope.credentials.consult.type[2].examplePrice = item;
        }
    });


    $scope.showNotDirtyInvalid = false;
    $scope.submit = function() {


        // console.log($scope.registerForm);
        // console.log($scope.registerForm.pickerBirthday.$error);

        $scope.showNotDirtyInvalid = true;
        if ($scope.registerForm.$valid) {
            $scope.loading = true;
            var values = $scope.credentials;
            values.excludeds = organizerModel.items;
            values.excludeds = organizerModel.items;
            values.teleconference = teleconference;
            values.teleconference = teleconference;
            values.avatarSrc = avatar.src;
            return users.register(values).success(function() {
                $scope.credentials = {};
                $state.go('dashboard.folders');
            }).error(function(jsonError) {
                $scope.errors = jsonError;
                // var jsonError = JSON.parse(data);
                if( jsonError !== false ){
                    for( var field in jsonError){
                        var item = jsonError[field];
                        for( var i in item){
                            var text = item[i];
                            utils.showToast(text);
                        }
                    }
                }
                
            }).finally(function() {
                $scope.loading = false;
            })
        }
    };

    $scope.setAvatar = function(fileData) {
        console.log( 'setAvatar' );
        avatar.loadData = fileData;
        $scope.openAvatarCropper();
    };

    $scope.getAvatar = function() {
        if( avatar.src == false )
            return avatar.src_default;
        else
            return avatar.src;
    };

    $scope.openAvatarCropper = function($event) {
        $mdDialog.show({
            template: $('#avatar-cropper-modal').html(),
            clickOutsideToClose: true,
            targetEvent: $event,
            controller: ['$scope', function($scope) {
                $scope.create = true;
                $scope.avatarCropperSrc = avatar.loadData;
                $scope.setCrop = function(){
                    avatar.src = cropperData;
                    $mdDialog.hide();
                }

                var cropperData = false;

                setTimeout(function(){
                    jQuery('#avatar-cropper-img').cropper({
                        aspectRatio: 1 / 1,
                        crop: function(e) {
                            var canvas = $(this).cropper('getCroppedCanvas', { width: 120, height: 120 });
                            cropperData = canvas.toDataURL();
                        }
                    });
                }, 300);

            }],
            disableParentScroll: false,
        });
    }


    $scope.organizerItems = organizerModel.items
    $scope.removeOrganizerItem = function( key ) {
        organizerModel.items.splice(key, 1);
    }

    $scope.openSelectDate = function($event) {
        $mdDialog.show({
            template: $('#organizer-calendar-select-date-modal').html(),
            clickOutsideToClose: true,
            targetEvent: $event,
            controller: ['$scope', function($scope) {
                $scope.create = true;
                $scope.addDateValue = false;
                $scope.addDate = function() {
                    var obj = new organizerModel.classes.type_1( this.addDateValue );
                    organizerModel.addDate( obj );
                    $mdDialog.hide();
                };
            }],
            disableParentScroll: false,
            onComplete: function() {
                console.log('complite');
            }
        });
    }

    $scope.openSelectSegment = function($event) {
        $mdDialog.show({
            template: $('#organizer-calendar-select-segment-modal').html(),
            clickOutsideToClose: true,
            targetEvent: $event,
            controller: ['$scope', function($scope) {
                $scope.create = true;
                $scope.addSegmentMinValue = false;
                $scope.addSegmentMaxValue = false;
                $scope.addSegment = function() {
                    var obj = new organizerModel.classes.type_2( this.addSegmentMinValue, this.addSegmentMaxValue );
                    organizerModel.addDate( obj );
                    $mdDialog.hide();
                };
            }],
            disableParentScroll: false,
            onComplete: function() {
                console.log('complite');
            }
        });
    }

    $scope.openSelectInterval = function($event) {
        $mdDialog.show({
            template: $('#organizer-calendar-select-interval-modal').html(),
            clickOutsideToClose: true,
            targetEvent: $event,
            controller: ['$scope', function($scope) {
                $scope.create = true;

                $scope.intervalWeek = [];
                $scope.intervalMonthWeek = [];
                $scope.intervalMonthDay = [];
                $scope.intervalYearMinDay = false;
                $scope.intervalYearMaxDay = false;
                $scope.weekDayName = weekDayName;

                $scope.toggle = function (item, list) {
                    var idx = list.indexOf(item);
                    if (idx > -1) {
                      list.splice(idx, 1);
                    } else {
                      list.push(item);
                    }
                };

                $scope.exists = function (item, list) {
                    return list.indexOf(item) > -1;
                };

                $scope.addIntervalWeek = function() {
                    var obj = new organizerModel.classes.type_3( $scope.intervalWeek );
                    organizerModel.addDate( obj );
                    $mdDialog.hide();
                };

                $scope.addIntervalMonthWeek = function() {
                    var list = {};
                    for( var key in $scope.intervalMonthWeek ) {
                        var item = $scope.intervalMonthWeek[key];
                        var subArr = explode( '_', item );
                        list[key] = subArr;
                    }
                    console.log( list );
                    var obj = new organizerModel.classes.type_5( list );
                    organizerModel.addDate( obj );
                    $mdDialog.hide();
                };

                $scope.addIntervalMonthDay = function() {
                    var obj = new organizerModel.classes.type_4( $scope.intervalMonthDay );
                    organizerModel.addDate( obj );
                    $mdDialog.hide();
                };

                $scope.addIntervalYeadDay = function() {
                    var obj = new organizerModel.classes.type_6( this.intervalYearMinDay, this.intervalYearMaxDay );
                    organizerModel.addDate( obj );
                    $mdDialog.hide();
                };

            }],
            disableParentScroll: false,
            onComplete: function() {
                console.log('complite');
            }
        });
    }

    function explode( delimiter, string ) { 
        var emptyArray = { 0: '' };

        if ( arguments.length != 2
            || typeof arguments[0] == 'undefined'
            || typeof arguments[1] == 'undefined' )
        {
            return null;
        }

        if ( delimiter === ''
            || delimiter === false
            || delimiter === null )
        {
            return false;
        }

        if ( typeof delimiter == 'function'
            || typeof delimiter == 'object'
            || typeof string == 'function'
            || typeof string == 'object' )
        {
            return emptyArray;
        }

        if ( delimiter === true ) {
            delimiter = '1';
        }

        return string.toString().split ( delimiter.toString() );
    }


    $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
          list.splice(idx, 1);
        } else {
          list.push(item);
        }
    };

    $scope.exists = function (item, list) {
        return list.indexOf(item) > -1;
    };

    $scope.openSelectTeleconfCalendar = function($event) {
        $mdDialog.show({
            template: $('#teleconf-calendar-modal').html(),
            clickOutsideToClose: true,
            targetEvent: $event,
            controller: ['$scope', function($scope) {
                $scope.create = true;
                $scope.dateMultiValue = [];
                $scope.select = function() {
                    for( var key in $scope.dateMultiValue ){
                        var item = $scope.dateMultiValue[key];
                        var obj = {
                            dateValue: item,
                            minTime: ($scope.minTime-1),
                            maxTime: $scope.maxTime,
                        };
                        teleconference.push( obj );
                        $mdDialog.hide();
                    }
                };
            }],
            disableParentScroll: false,
            onComplete: function() {
                console.log('complite');
            }
        });
    }

    $scope.removeTeleconferenceItem = function(key) {
        teleconference.splice(key, 1);
    }

    $scope.openAGB = function($event) {
        $mdDialog.show({
            template: $('#text-agb-modal').html(),
            clickOutsideToClose: true,
            targetEvent: $event,
            controller: ['$scope', function($scope) {
                $scope.create = true;
            }],
        });
    }

}]);
