<!DOCTYPE html>
<html lang="en" ng-app="StarterApp">
    <head>

        {{-- Set base url if admin has enabled HTML5 push state --}}
        @if ($settings->get('enablePushState'))
            <base href="{{ $pushStateRootUrl }}">
        @endif

        <title>{{ $settings->get('siteName') }}</title>

        {{-- Meta --}}
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="{{ $settings->get('metaDescription') }}" />
        <meta name="title" content="{{ $settings->get('metaTitle') }}" />

        {{-- CSS --}}
        <!-- <link rel="stylesheet" href="{{ asset('assets/css/styles.min.css?v12') }}"> -->
        {{-- Fonts --}}
        <link href='https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,900' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

        <link rel="stylesheet" type="text/css" href="{{ asset('assets/new/custom.css') }}">

    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.css">

        {{-- Favicons --}}

        <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">

        <link rel="manifest" href="favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <script type="text/javascript">
            var debuggerScope = {};
        </script>
    </head>

    <body layout="row" ng-controller="AppCtrl">

        <div ng-include="'assets/new/register.html'"/>

        <script id="vars">
            var vars = {
                user: '{!! $user !!}',
                baseUrl: '{{ $baseUrl  }}',
                selectedLocale: '{{ Config::get('app.locale') }}',
                trans: {!! $translations !!},
                settings: {!! json_encode($settings->getAll()) !!},
                isDemo: '{{ $isDemo  }}'
            }
        </script>

        <!-- <script src="{{ asset('assets/js/core.min.js?v=1') }}"></script> -->

        @if (($locale = $settings->get('dateLocale', 'en')) && $locale !== 'en')
            <script src="{{ asset('assets/js/locales/'.$locale.'.js')  }}"></script>
        @endif


            <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular.min.js"></script>
            <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-animate.min.js"></script>
            <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular-aria.min.js"></script>

            <script src="//ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.js"></script>
            
            <script src="//cdn.jsdelivr.net/angular-material-icons/0.4.0/angular-material-icons.min.js"></script> 
            <script src="{{ asset('assets/new/app.js')  }}"></script>
            <script src="{{ asset('assets/new/angular-translate.js')  }}"></script>
            <script src="{{ asset('assets/new/angular-translate-url-loader.js')  }}"></script>

        @if ($code = $settings->get('analytics'))

        @endif
    </body>
</html>
