'use strict';

angular.module('app')

.directive('edTooltip', function() {
    return {
        restrict: 'A',
        link: function($scope, el, attrs) {

            // console.log( el );
            //no need for tooltips on phones
            if ($scope.isSmallScreen) return;

            el.tooltip({
                placement: attrs.tooltipPlacement || 'bottom',
                delay: 50,
                content: attrs.edTooltip,
                container: 'body'
            })
        }
    }
});