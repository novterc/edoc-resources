'use strict';

angular.module('app').filter('range', function() {
  return function(input, v1, v2) {

  	v1 = parseInt(v1);
  	if( v2 !== undefined ){
  		v2 = parseInt(v2);

  		var start = v1;
  		var fin = v2;

  	} else {
  		var start = 0;
  		var fin = v1;
  	}

  	if( start < fin ){
	    for (var i=start; i<fin; i++) {
	      input.push(i);
	    }
	} else {
		for (var i=start; i>fin; i--) {
	      input.push(i);
	    }
	}

    return input;
  };
});