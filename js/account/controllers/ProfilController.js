'use strict';

angular.module('pixie.dashboard').controller('ProfilController', ['$rootScope', '$scope', '$http', '$state', 'users', '$mdDialog', 'utils', function($rootScope, $scope, $http, $state, users, $mdDialog, utils) {
    
	$scope.profilCompliteNumber = 51;

    $scope.credentials = {};


    var avatar = {
        src_default: users.getAvatar(),
        src: false,
        loadData: false,
    }    

    $http.get('specializations').success(function (data) {
    }).then(function (responce) {
        $scope.specializations = responce['data'];
        $scope.specializationsName = lists(responce['data'], 'id', 'name');
    });

    $http.get('languages').success(function (data) {
    }).then(function (responce) {
        $scope.languages = responce['data'];
        $scope.languagesName = lists(responce['data'], 'id', 'name');
    });

    $http.get('countries').success(function (data) {
    }).then(function (responce) {
        $scope.countries = responce['data'];
    });

    $http.get('workings').success(function (data) {
    }).then(function (responce) {
        $scope.workings = responce['data'];
    });

    $http.get('workings').success(function (data) {
    }).then(function (responce) {
        $scope.workings = responce['data'];
        $scope.workingsName = lists(responce['data'], 'id', 'name');
    });



    
    $http.get('/getDoctorInfo').success(function (data) {
    }).then(function (responce) {
        $scope.credentials = responce['data'];
    });
    

    $scope.showNotDirtyInvalid = false;
    $scope.submit = function() {


        // console.log($scope.registerForm);
        // console.log($scope.registerForm.pickerBirthday.$error);

        $scope.showNotDirtyInvalid = true;
        if ($scope.registerForm.$valid) {
            $scope.loading = true;
            var values = $scope.credentials;
            values.avatarSrc = avatar.src;

            return $http.post('/setDoctorInfo', values).success(function() {
                // $scope.credentials = {};
                // $state.go('dashboard.folders');
                utils.showToast('success update');
            }).error(function(jsonError) {
                utils.showToast('error');
                // $scope.errors = jsonError;
                // // var jsonError = JSON.parse(data);
                // if( jsonError !== false ){
                //     for( var field in jsonError){
                //         var item = jsonError[field];
                //         for( var i in item){
                //             var text = item[i];
                //             utils.showToast(text);
                //         }
                //     }
                // }                
            }).finally(function() {
                $scope.loading = false;
            })
        }
    };

    $scope.setAvatar = function(fileData) {
        console.log( 'setAvatar' );
        avatar.loadData = fileData;
        $scope.openAvatarCropper();
    };

    $scope.getAvatar = function() {
        if( avatar.src == false )
            return avatar.src_default;
        else
            return avatar.src;
    };

    $scope.openAvatarCropper = function($event) {
        $mdDialog.show({
            template: $('#avatar-cropper-modal').html(),
            clickOutsideToClose: true,
            targetEvent: $event,
            controller: ['$scope', function($scope) {
                $scope.create = true;
                $scope.avatarCropperSrc = avatar.loadData;
                $scope.setCrop = function(){
                    avatar.src = cropperData;
                    $mdDialog.hide();
                }

                var cropperData = false;

                setTimeout(function(){
                    jQuery('#avatar-cropper-img').cropper({
                        aspectRatio: 1 / 1,
                        crop: function(e) {
                            var canvas = $(this).cropper('getCroppedCanvas', { width: 120, height: 120 });
                            cropperData = canvas.toDataURL();
                        }
                    });
                }, 300);

            }],
            disableParentScroll: false,
        });
    }



    function lists(arr, keyName, valueName) {
        var result = {};
        for(var key in arr ){
            var item = arr[key];
            result[item[keyName]] = item[valueName];
        }
        return result;
    }

}]);
