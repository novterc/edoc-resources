'use strict';

angular.module('app')

.directive('edSetScopeEl', ['$rootScope', function($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            scope.addEl(attrs.edSetScopeEl, elem );
        }
    };
}])
.directive('edDraggable', ['$rootScope', function($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {

            $rootScope.$on('mri.serial.select', function() {
                elem.css('left', '0px');
                elem.css('top', '0px');             
            });

            elem.draggable();
        }
    };
}])
.directive('edRuler', ['$rootScope', 'mri', function($rootScope, mri) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) { 
            
            $rootScope.$on('mri.previewList.loaded', function() {
                draw();
            });
            $rootScope.$on('mri.image.select', function() {
                draw();
            });
            $rootScope.$on('mri.image.changeZoom', function() {
                draw();
            });

            function draw() {
                if(attrs.spin == 'Y')           
                    drawY();

                if(attrs.spin == 'X')           
                    drawX();
            }
            

            function drawY() {
                var elemI = elem[0];

                if( !mri.selectedImage || mri.selectedImage.pixel_spacing_rows == undefined )
                    return;

                if( scope.bottomEl != undefined )
                    elemI.height = scope.bottomEl.height();

                
                var pixelSpacing = mri.selectedImage.pixel_spacing_rows;                
                var pixOne = Math.round(1 / pixelSpacing);
                pixOne = correctorByZoom(pixOne);     
                
                var ctx = elemI.getContext("2d");                        
                ctx.clearRect(0, 0, elemI.width, elemI.height);

                ctx.strokeStyle = "green";
                ctx.lineWidth = 2;

                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(0, pixOne*100);
                ctx.stroke();

                for (var s = 0; s <= 10; s++) {
                    for (var m = 0; m <= 10; m++) {

                        var lev = pixOne*(m+(s*10));
                        var leng = 2;
                        if( m == 0 )
                            leng = 10;

                        if( s == 5 && m == 0 )
                            leng = 20;

                        if( s == 10 && m == 1 )
                            break;

                        ctx.beginPath();
                        ctx.moveTo(0, lev);
                        ctx.lineTo(leng, lev);
                        ctx.stroke();

                    }
                }
            }


            function drawX() {
                if( !mri.selectedImage || mri.selectedImage.pixel_spacing_columns == undefined )
                    return;

                if( scope.bottomEl != undefined )
                    elem[0].width = scope.bottomEl.width();
                
                var pixelSpacing = mri.selectedImage.pixel_spacing_columns;                
                var pixOne = Math.round(1 / pixelSpacing ); 
                pixOne = correctorByZoom(pixOne);     
                

                var ctx = elem[0].getContext("2d");                        
                ctx.clearRect(0, 0, elem[0].width, elem[0].height);

                ctx.strokeStyle = "green";
                ctx.lineWidth = 2;

                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(pixOne*100, 0);
                ctx.stroke();

                for (var s = 0; s <= 10; s++) {
                    for (var m = 0; m <= 10; m++) {

                        var lev = pixOne*(m+(s*10));
                        var leng = 2;
                        if( m == 0 )
                            leng = 10;

                        if( s == 5 && m == 0 )
                            leng = 20;

                        if( s == 10 && m == 1 )
                            break;

                        ctx.beginPath();
                        ctx.moveTo(lev, 0);
                        ctx.lineTo(lev, leng);
                        ctx.stroke();

                    }
                }
            }

            function correctorByZoom(pixOne) {
                return pixOne+((pixOne/100)*mri.zoomRange);
            }

        }
    };
}]);