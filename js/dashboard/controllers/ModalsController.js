'use strict';

angular.module('pixie.dashboard').controller('ModalsController', ['$rootScope', '$scope',
    function ($rootScope, $scope) {
        /**
         * Shows limit message to multiple selects
         *
         * @param $select
         * @param el
         */
        $scope.checkLimit = function ($select, el) {
            if($select.selected.length >= $select.limit){
                if(!document.querySelector('div[name='+el.ngModel.$name+'] .limit_error')){
                    var text = document.createElement('div');
                    text.className = 'limit_error form-error help-block animated flipInX';
                    text.innerHTML ="Maximum selected values is 3. Remove previous values to select another one.";
                    document.querySelector('md-dialog div[name='+el.ngModel.$name+']').appendChild(text);
                }
            }
        };
    }]);