'use strict';

angular.module('pixie.dashboard')

.directive('fileButton', function() {
    return {
        link: function(scope, element, attributes, ctrl) {

            var el = angular.element(element)
            var button = el.children()[0]

            el.css({
                position: 'relative',
                overflow: 'hidden',
                width: button.offsetWidth,
                height: button.offsetHeight
            })

            var fileInput = angular.element('<input type="file" accept="image/x-png,image/jpeg" />')
            fileInput.css({
                position: 'absolute',
                top: 0,
                left: 0,
                'z-index': '2',
                width: '100%',
                height: '100%',
                opacity: '0',
                cursor: 'pointer'
            })

            el.append(fileInput)

            var functionName = attributes.selectFile;
                fileInput.change(function(){
                readURL(this);
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        scope[functionName]( e.target.result );
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

        }
    }
})