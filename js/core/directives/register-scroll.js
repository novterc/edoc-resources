angular.module('app').directive('registerScroll', function() {
    return {
        scope: {
            scroll: "&"
        },
        link: function($scope, element, attrs) {
            var not_scrolled = true;
            element.scroll(function() {
                $scope.$apply(function() {
                    if (document.querySelectorAll('#login-page .form-error').length && document.querySelector('#login-page .form-error').style.display != 'none') {
                        if (not_scrolled) {
                            not_scrolled = false;
                            var els = document.querySelectorAll('#login-page .form-error');
                            for (var x = 0; x < els.length; x++) {
                                els[x].style.opacity = 1;
                            }
                        }

                        var timerId = setInterval(function () {
                            var els = document.querySelectorAll('#login-page .form-error');
                            for (var x = 0; x < els.length; x++) {
                                els[x].style.opacity = els[x].style.opacity / 1.05;
                            }
                        }, 100);

                        setTimeout(function () {
                            clearInterval(timerId);
                            var els = document.querySelectorAll('#login-page .form-error');
                            for (var x = 0; x < els.length; x++) {
                                els[x].style.opacity = 1;
                                els[x].style.display = 'none';
                                not_scrolled = true;
                            }
                        }, 1000);
                    }
                })
            })
        }
    }
});